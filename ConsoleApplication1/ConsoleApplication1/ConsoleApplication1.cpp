struct Karta
{
string nazwa;
string kolor;
Karta *next;
Karta *prev;
 
};
 
Karta *dopisz(Karta *p, string nazwa, string kolor)
{
Karta *a = new Karta;
p->next = a;
a->nazwa = nazwa;
a->kolor = kolor;
a->prev = p;
a->next = NULL;
 
return a;
}
 
Karta *zbudujTalie()
{
int el = 4 * 13;
Karta *head = new Karta;
Karta *tail = head;
head->prev = NULL;
head->nazwa = "";
head->kolor = "";
 
for (int i = 1; i < el; i++)
tail = dopisz(tail, "", "");
 
return head;
 
}
 
void wyswietlTalie(Karta *ls)
{
Karta *n = ls;
int i = 0;
 
while (n->next != NULL)
{
Karta *ak = n;
cout << i << ". " << n->nazwa << " " << n->kolor << endl;
n = ak->next;
i++;
}
 
 
}