#include <iostream>
#include <vector>
#include <conio.h>
using namespace std;
#pragma once
class Ludzik 
{
	char symbol;
public:
	int x, y;
	Ludzik();
	~Ludzik();
	void SetLudzik(char a);
	void SetX(int n);
	void SetY(int n);
	char GetSymbol();
	int ModX(int a);
	int ModY(int b);
	void PzesunSie(Ludzik plansza[20][20]);
};

