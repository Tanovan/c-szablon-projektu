#include "Ludzik.h"
#include "Plansza.h"

Ludzik::Ludzik()
{
}

Ludzik::~Ludzik()
{
};

void Ludzik::SetLudzik(char a)
{
	symbol = a;
}

void Ludzik::PzesunSie(Ludzik plansza[20][20])
{
		switch (getch())
		{
		case 'w':
			ModY(y);
			plansza[x][y + 1] = plansza[x][y];
			plansza[x][y].SetLudzik(' ');
			plansza[x][y + 1].SetY(y + 1);
			break;
		case 's':
			ModY(y);
			plansza[x][y - 1] = plansza[x][y];
			plansza[x][y].SetLudzik(' ');
			plansza[x][y - 1].SetY(y - 1);
			break;
		case 'a':
			ModY(x);
			plansza[x-1][y] = plansza[x][y];
			plansza[x][y].SetLudzik(' ');
			plansza[x-1][y].SetX(x - 1);
			break;
		case 'd':
			ModY(x);
			plansza[x+1][y] = plansza[x][y];
			plansza[x][y].SetLudzik(' ');
			plansza[x+1][y].SetX(x + 1);
			break;
		default:
			break;
		}
	}
};

void Ludzik::SetX(int n)
{
	x = n;
}

void Ludzik::SetY(int n)
{
	y = n;
}

char Ludzik::GetSymbol()
{
	return symbol;
}

int Ludzik::ModX(int a)
{
	if (a != 0)
		return (a) % 21;
	else
		return 21;
};

int Ludzik::ModY(int b)
{
	if (b != 0)
		return (b) % 21;
	else
		return 21;
};